from django.contrib import admin

from .models import Userprofile, Document, Uploaddata

admin.site.register(Document)
admin.site.register(Userprofile)
admin.site.register(Uploaddata)