from django import forms
from django.contrib.auth.forms import UserCreationForm, User
from django.contrib.auth.forms import AuthenticationForm
from core.models import Document, Uploaddata

# Create your forms here...
Approval_CHOICES= [
    ('uploaded', 'Uploaded'),
    ('approved', 'Approved'),
    ('rejected', 'Rejected'),
    ('pending', 'Pending'),
    ]

class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['username','first_name','last_name','email','password']

class DocumentForm(forms.ModelForm):
    class Meta:
        model = Uploaddata
        #fields = '__all__'
        fields = ('userid', 'name', 'description', 'image', )