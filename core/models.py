from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone

class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Userprofile(models.Model):
    username = models.CharField(max_length=100, null=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, unique=True)
    password = models.CharField(max_length=200)
    def __str__(self):
        return self.first_name

class Uploaddata(models.Model):
    userid = models.OneToOneField(User, on_delete=models.CASCADE, related_name="uploaddata")
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    #image = models.CharField(max_length=100, blank=True)
    image = models.ImageField(upload_to="image")
    status = models.CharField(max_length=100)
    def __str__(self):
        return self.name

class Document(models.Model):
    userid = models.CharField(max_length=10, blank=True)
    name = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=255, blank=True)
    document = models.CharField(max_length=100, blank=True)
    status = models.EmailField(max_length=100)
    def __str__(self):
        return self.name
