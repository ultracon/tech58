from django.shortcuts import get_object_or_404, render, redirect, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from core.models import Uploaddata
from .forms import UserForm, DocumentForm
from django.contrib import messages


#Home Page Of Core App
@login_required(login_url='/accounts/login/')
def index(request):
    user = User.objects.all()
    context = { 'user': user }
    return render(request, 'core/index.html', context)

#Update/Edit User Recoord
@login_required(login_url='/accounts/login/')
def update_user(request,id):
    if request.method == "POST":
        u_user = User.objects.get(pk=id)
        fom = UserForm(request.POST, instance=u_user)
        if fom.is_valid():
            fom.save() 
    else:
        u_user = User.objects.get(pk=id)
        fom = UserForm(instance=u_user)
    return render(request, 'core/update-user.html',{'form':fom, 'u_user':u_user})

#Delete User data
@login_required(login_url='/accounts/login/')
def delete(request, id):
    all_user = User.objects.all()
    if request.method == "POST":
        de = User.objects.get(pk=id)
        de.delete()
    return render(request, 'core/register.html', {'all_user':all_user})
        
#Register New User
@login_required(login_url='/accounts/login/')
def user_register(request):
    all_user = User.objects.all()
    if request.method == "POST":
        fom = UserForm(request.POST)
        if fom.is_valid():
            fom.save()  
            messages.add_message(request, messages.SUCCESS, 'Your Acount has been created!')      
    else:
        fom = UserForm()
    return render(request, 'core/register.html', {'form': fom, 'all_user':all_user})

#All Users
@login_required(login_url='/accounts/login/')
def users_detail(request):
    all_user = User.objects.all()
    return render(request, 'core/users.html', {'all_user':all_user})

#Upload User 
@login_required(login_url='/accounts/login/')
def upload_documents(request):
    emps = User.objects.all()
    all_docs = Uploaddata.objects.all()
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        #import pdb; pdb.set_trace()
        #print(form)
        if form.is_valid():
            form.save()
            return redirect('documents')
    else:
        form = DocumentForm()
    return render(request, 'core/upload-documents.html', {'form': form, 'all_docs':all_docs, 'emps':emps})