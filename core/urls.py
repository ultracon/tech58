from django.contrib import admin
from django.urls import path, re_path
from core import views

urlpatterns = [
    path('', views.index, name='home'),
    path('users_detail', views.users_detail, name='users_detail'),
    path('documents', views.upload_documents, name='documents'),
    path('register', views.user_register, name='register'),
    path('delete/<int:id>', views.delete, name= 'delete'),
    path('update/<int:id>', views.update_user, name= 'updateuser'),
]