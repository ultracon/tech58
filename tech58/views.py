from django.shortcuts import get_object_or_404, render, redirect, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.contrib import messages


#Home Page Of Core App
def index(request):
    user = User.objects.all()
    context = { 'user': user }
    return render(request, 'core/index.html', context)

#Login Registered User
def user_login(request):
    context={}
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)  
        #import pdb; pdb.set_trace()
        #print('hello', user)            
        if user is not None:
            login(request, user)
            return render(request, 'index.html')
        else:
            context["error"] = "Provide Valid Credintial !!"
            return render(request, 'login.html', context)
    else:
        return render(request, 'login.html', context)  

#Logout User
def user_logout(request):
    auth.logout(request)
    messages.info(request, "You have successfully logged out.")  
    return HttpResponseRedirect('login')
        
